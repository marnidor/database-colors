-- MySQL dump 10.13  Distrib 8.0.30, for macos12.4 (x86_64)
--
-- Host: localhost    Database: colors
-- ------------------------------------------------------
-- Server version	8.0.30

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `animals`
--

DROP TABLE IF EXISTS `animals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `animals` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `color` varchar(50) NOT NULL DEFAULT '#FFFFFF',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `animals`
--

LOCK TABLES `animals` WRITE;
/*!40000 ALTER TABLE `animals` DISABLE KEYS */;
INSERT INTO `animals` VALUES (24,'Rio','line','#483D8B'),(44,'Mary','tiger','#483D8B'),(45,'Ann','line','#FFAAFF'),(46,'Rio','tiger','#FFFFFF'),(47,'Ann','line','#FFAAFF'),(48,'Rio','tiger','#FFFFFF'),(49,'Ann','line','#FFAAFF'),(51,'Ann','line','#FFAAFF'),(52,'Rio','tiger','#FFFFFF'),(53,'Ann','line','#FFAAFF'),(54,'Rio','tiger','#FFFFFF'),(55,'Ann','line','#FFAAFF'),(56,'Rio','tiger','#FFFFFF'),(57,'Ann','line','#FAAAFF'),(58,'Rio','tiger','#FFFFFF'),(59,'Ann','line','#FAAAFF'),(60,'Rio','tiger','#FFFFFF'),(61,'Ann','line','#FAAAFF'),(62,'Rio','tiger','#FFFFFF'),(63,'Ann','line','#FAAAFF'),(64,'Rio','tiger','#FFFFFF'),(65,'Ann','line','#FAAAFF'),(66,'Rio','tiger','#FFFFFF'),(67,'Ann','line','#FAAAFF'),(68,'Rio','tiger','#FFFFFF'),(69,'Ann','line','#FAAAFF'),(70,'Rio','tiger','#FFFFFF'),(71,'Ann','line','#FAAAFF'),(72,'Rio','tiger','#FFFFFF'),(73,'Ann','line','#FAAAFF'),(75,'Ann','line','#FAAAFF'),(76,'Rio','tiger','#FFFFFF'),(77,'Ann','line','#FAAAFF'),(78,'Rio','tiger','#FFFFFF'),(79,'Ann','line','#FAAAFF'),(80,'Rio','tiger','#FFFFFF'),(81,'Ann','line','#FAAAFF'),(82,'Rio','tiger','#FFFFFF'),(83,'Ann','line','#FAAAFF'),(84,'Rio','tiger','#F0E68C'),(86,'Rio','tiger','#FFFFFF'),(87,'Ann','line','#FAAAFF'),(88,'Rio','tiger','#FFFFFF'),(89,'Ann','line','#FFFF00'),(90,'Pam','dog','#B8860B'),(103,'Sam','camel','#FFFFFF'),(104,'fekla','cow','#FFFFFF'),(105,'fekla','cow','#FF7F50'),(106,'Pam','dog','#B8860B'),(107,'fekla','camel','#FFFFFF'),(108,'Pam','dog','#FF7F50');
/*!40000 ALTER TABLE `animals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `main_table`
--

DROP TABLE IF EXISTS `main_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `main_table` (
  `id_main_table` int NOT NULL AUTO_INCREMENT,
  `name_table_DB` varchar(50) DEFAULT NULL,
  `user_name_table` varchar(50) DEFAULT NULL,
  `column_name_1` varchar(50) DEFAULT NULL,
  `column_number_1` int DEFAULT NULL,
  `column_name_2` varchar(50) DEFAULT NULL,
  `column_number_2` int DEFAULT NULL,
  `column_name_3` varchar(50) DEFAULT NULL,
  `column_number_3` int DEFAULT NULL,
  `column_name_4` varchar(50) DEFAULT NULL,
  `column_number_4` int DEFAULT NULL,
  PRIMARY KEY (`id_main_table`),
  UNIQUE KEY `main_table_id_main_table_uindex` (`id_main_table`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `main_table`
--

LOCK TABLES `main_table` WRITE;
/*!40000 ALTER TABLE `main_table` DISABLE KEYS */;
INSERT INTO `main_table` VALUES (1,'animals','Животные моего зоопарка','type',2,'name',3,'id',1,NULL,NULL),(2,'students','Группа 45/14','id',1,'surname',4,'name',3,'number',2);
/*!40000 ALTER TABLE `main_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students`
--

DROP TABLE IF EXISTS `students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `students` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `surname` varchar(50) DEFAULT NULL,
  `number` varchar(50) DEFAULT NULL,
  `color` varchar(50) NOT NULL DEFAULT '#FFFFFF',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students`
--

LOCK TABLES `students` WRITE;
/*!40000 ALTER TABLE `students` DISABLE KEYS */;
INSERT INTO `students` VALUES (1,'Ann','Brown','12345','#FFFFFF'),(2,'Mary','Brown','123456','#FAAAFF'),(4,'Mary','Brown','123456','#FAAAFF'),(6,'Mary','Brown','123456','#FAAAFF'),(7,'Ann','Brown','12345','#FFFFFF'),(8,'Mary','Brown','123456','#FAAAFF'),(10,'Mary','Brown','123456','#FAAAFF'),(11,'Ann','Brown','12345','#FFFFFF'),(12,'Ann','Brown','12345','#FFFFFF'),(13,'Mary','Brown','123456','#FAAAFF'),(14,'Man','Brown','12345','#7FFFD4'),(15,'Ann','Brown','12345','#FFFFFF'),(16,'Mary','Brown','123456','#FAAAFF'),(17,'Ann','Brown','12345','#FFFFFF'),(19,'Mary','Brown','123456','#DC143C'),(20,'Ann','Brown','12345','#FFFFFF'),(22,'Mary','Brown','123456','#DC143C'),(23,'Pam','Pavlenko','1111','#FFFFFF'),(24,'Pam','Ivanov','0000000','#FF7F50');
/*!40000 ALTER TABLE `students` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-10-13 15:18:13
