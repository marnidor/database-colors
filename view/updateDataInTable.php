<?php

require_once 'head.php';

$dataRow = \App\database\DataFromDB::getOneRowFromTable($_SESSION['tableName'], $_GET['id']);
$color = $dataRow['color'];
$_SESSION['id'] = $_GET['id'];
$columns = \App\database\DataFromDB::getColumnsFromTable($_SESSION['tableName']);

?>
<form action="updateInsert" method="post">
    <h1 class="text-center">Database: <?=ucfirst($_SESSION['tableName']);?> </h1>

    <table  class="table table-bordered table-hover w-50 offset-md-3">
        <thead>
        <tr>
            <?php
            foreach ($dataRow as $key => $value)  { ?>
                <th name="$key" scope="col"><?=ucfirst($key);?></th>
            <?php } ?>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th name="th" scope="row" style="background-color: <?=$dataRow['color'];?>"><?=$_GET['id'];?></th>
            <?php
            foreach ($dataRow as $key => $value) {
                if ($key === 'Id') {
                    continue;
                }
                ?>

                <td style="background-color: <?=$color;?>"><input name="<?=$key;?>" value="<?=$value;?>"></td>
                    <?php
                ?>

            <?php } ?>
        </tr>
        </tbody>
    </table>
    <button type="submit" class="btn btn-outline-primary offset-md-3" value="<?=$_SESSION['id'];?>">Сохранить</a></button>
</form>


