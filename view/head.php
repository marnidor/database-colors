<?php
$tables = \App\database\DataFromDB::getTablesFromDB();

?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Database</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <!-- Bootstrap Bundle JS (jsDelivr CDN) -->
    <script defer src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</head>
<body>
    <form method="post" action="showDatabase">
        <select name="select" class="form-select form-select-lg mb-3 w-25 offset-md-2 mt-3" aria-label=".form-select-lg" required>
            <option selected>Выбрать имя базы данных</option>
            <?php
            foreach ($tables as $key => $value) {
                if ($value === 'main_table') {
                    continue;
                }
                ?>

                <option value="<?=$value;?>"> <?=$value;?> </option>
                <?php
            }
            ?>
        </select>
        <button type="submit" class="btn btn-outline-secondary offset-md-2">Открыть</button>
    </form>
</body>
</html>


