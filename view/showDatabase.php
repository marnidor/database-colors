<?php

require_once 'head.php';

if (empty($_POST['select'])) {
     $_POST['select'] = $_SESSION['tableName'];
} else {
    $_SESSION['tableName'] = $_POST['select'];
}

$columns = \App\database\DataFromDB::getColumnsFromTable($_SESSION['tableName']);
$dataFromTable = \App\database\DataFromDB::getDataFromTable($_SESSION['tableName']);
$columnsNew = \App\database\GetDataFromDBWithColumns::getTheColumnsInTheCorrectOrder($_SESSION['tableName']);

foreach ($columnsNew as $key => $value) {
    $keysNew[] = array_search($value, $columns);
}
?>
    <input name="nameDB" value="<?=$_SESSION['tableName'];?>" type="hidden">
    <h1 class="text-center">Database: <?=ucfirst($_SESSION['tableName']);?> </h1>
    <h2 class="text-center"><?=\App\database\GetDataFromDBWithColumns::getUserTableName($_SESSION['tableName']);?></h2>

    <table class="table table-bordered table-hover w-50 offset-md-3">
        <thead>
        <tr>
        <?php
            foreach ($columnsNew as $column) { ?>
                <th name="th" scope="col"><?=ucfirst($column);?></th>
    <?php } ?>
        </tr>
        </thead>
        <tbody>
        <tr>
        <?php
            foreach ($dataFromTable as $key => $data) {?>
            <th name="th" scope="row" style="background-color: <?=$data[count($data) - 1];?>"><?=$data[0];?></th>
    <?php
                for ($i = 1; $i <= count($keysNew) - 1; $i++) {
                    ?>
                    <td style="background-color: <?=$data[count($data) - 1];?>" ><?=$data[$keysNew[$i]];?></td>
                <?php } ?>
            <td>
                <form method="post">
                    <button value="<?=$_SESSION['tableName'];?>" name="nameDB" type="submit" class=" btn btn-lg btn-outline-warning"><a class="text-decoration-none text-reset" href="/updateDataInTable?id=<?= $data[0];?>">Изменить</a></button>
                </form>
            </td>
            <td>
                <button name="<?=$_SESSION['tableName'];?>" type="submit" class=" btn btn-lg btn-outline-danger"><a class="text-decoration-none text-reset" href="/deleteRow?id=<?= $data[0];?>">Удалить</a></button>
            </td>
        </tr>
           <?php } ?>
        </tbody>
    </table>
    <form method="post" action="addNewRow">
        <button value="<?=$_SESSION['tableName'];?>" name="nameDB" type="submit" class="w-50 offset-md-3 btn btn-lg btn-outline-success"><a class="text-decoration-none text-reset" href="/addNewRow">Добавить запись</a></button>
    </form>

<?php
$_SESSION['temp']++;
?>

