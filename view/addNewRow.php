<?php

require_once 'view/head.php';

$columns = \App\database\DataFromDB::getColumnsFromTable($_SESSION['tableName']);
$_SESSION['columns'] = $columns;
?>

<form action="addInsert" method="post">
    <h1 class="text-center">Database: <?=ucfirst($_SESSION['tableName']);?> </h1>

    <table  class="table table-bordered table-hover w-50 offset-md-3">
        <thead>
        <tr>
            <?php
            foreach ($columns as $key => $value)  {
                if ($value === 'Id') {
                    continue;
                }
                ?>

                <th name="$key" scope="col"><?=ucfirst($value);?></th>
            <?php } ?>
        </tr>
        </thead>
        <tbody>
        <tr>
<!--            <th name="th" scope="row" style="background-color: --><?//=$dataRow['color'];?><!--">--><?//=$_GET['id'];?><!--</th>-->
            <?php
            foreach ($columns as $key => $value) {
                if ($value === 'Id') {
                    continue;
                }
                ?>

                <td><input name="<?=$key;?>"></td>
                <?php
                ?>

            <?php } ?>
        </tr>
        </tbody>
    </table>
    <button type="submit" class="btn btn-outline-primary offset-md-3">Добавить</a></button>
</form>
