<?php

$newRow = new \App\database\CreateTableInDB();
$columns = $_SESSION['columns'];
unset($columns[0]);

if ($_POST[count($_POST)] === '') {
    unset($_POST[count($_POST)]);
    unset($columns[count($columns)]);
}

$columns = implode(', ', $columns);

$newRow->insertValuesIntoTable($_SESSION['tableName'], $_POST, $columns);
\App\services\Router::openPage('/showDatabase', 'showDatabase');
