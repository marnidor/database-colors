<?php

namespace App\database;
use mysqli;

class ConnectDB
{
    public static function start(): void
    {
        self::connectDB();
    }

    protected static function connectDB()
    {
        $connect = new mysqli('localhost', 'marnidor', 'password', 'colors');
        if ($connect->connect_error) {
            die("Ошибка: " . $connect->connect_error);
        }
        return $connect;
    }
}