<?php

namespace App\database;
use App\services\Router;

class GetDataFromDBWithColumns extends ConnectDB
{
    public static function getTheColumnsInTheCorrectOrder(string $nameTable)
    {
        $connect = ConnectDB::connectDB();
        $query = $connect->query("SELECT * FROM colors.main_table WHERE name_table_DB = '$nameTable'");
        $elements = $query->fetch_assoc();

        foreach ($elements as $key => $value) {
            if (is_int(strripos($key, 'column_name')) && !is_null($value)) {
                $columnName[] = $value;
            } elseif (is_int(strripos($key, 'column_number')) && !is_null($value)) {
                $columnNumber[] = $value;
            }
        }

        $temp = $columnName;
        foreach ($columnNumber as $key => $value) {
            $temp[(int)$value - 1] = $columnName[$key];
        }
        $columnName = $temp;
        $columnName[] = 'color';

        return $columnName;
    }

    public static function getUserTableName(string $nameTable)
    {
        $connect = ConnectDB::connectDB();
        $query = $connect->query("SELECT * FROM colors.main_table WHERE name_table_DB = '$nameTable'");
        $elements = $query->fetch_assoc();
        return $elements['user_name_table'];
    }
}