<?php

namespace App\database;
use App\services\Router;

class DataFromDB extends ConnectDB
{
    public static function getTablesFromDB(): array
    {
        $connect = ConnectDB::connectDB();
        $query = $connect->query("SHOW TABLES FROM colors");

        $tables = [];
        while (($row = $query->fetch_array())) {
            $tables[] = $row[0];
        }

        return $tables;
    }

    public static function getColumnsFromTable(string $nameTable): array
    {
        $connect = ConnectDB::connectDB();

        $query = $connect->query("SHOW COLUMNS FROM colors.$nameTable");
        $columns = [];
        while (($row = $query->fetch_array())) {
            $columns[] = $row[0];
        }
        return $columns;
    }

    public static function getDataFromTable(string $nameTable): array
    {
        $connect = self::connectDB();
        $query = $connect->query("SELECT * FROM colors.$nameTable");
        return $query->fetch_all();
    }

    public static function updateDataInTable(string $nameTable, int $id, $post)
    {
        $connect = self::connectDB();
        $columns = self::getColumnsFromTable($nameTable);

        foreach ($columns as $key => $column) {
            if ($column !== 'Id') {
                $query = $connect->query("UPDATE colors.$nameTable SET $column = '$post[$column]' WHERE id = '$id'");
            }
        }

        Router::redirectPage('showDatabase');
    }

    public static function deleteRowInTable(int $id, string $nameTable)
    {
        $connect = ConnectDB::connectDB();

        $connect->query("DELETE FROM colors.$nameTable WHERE id = '$id'");
        Router::redirectPage('showDatabase');
    }

    public static function getOneRowFromTable(string $nameTable, int $id)
    {
        $connect = ConnectDB::connectDB();

        $query = $connect->query("SELECT * FROM colors.$nameTable WHERE id = '$id'");
        $result = $query->fetch_assoc();
        require_once 'view/updateDataInTable.php';
        return $result;
    }
}
