<?php

namespace App\database;

use App\services\Router;

class CreateTableInDB extends ConnectDB
{
    private static function getColumnsName(string $nameColumns)
    {
        return explode(", ", $nameColumns);
    }

    public function createTableInDB(string $nameTable, string $nameColumns, int $amountOfColumns)
    {
        $connect = ConnectDB::connectDB();

        $connect->query("CREATE TABLE colors.$nameTable (Id INT AUTO_INCREMENT PRIMARY KEY)");
        $columns = self::getColumnsName($nameColumns);

        for ($i = $amountOfColumns - 2; $i >= 0 ; $i--) {
            $connect->query("ALTER TABLE colors.$nameTable ADD COLUMN $columns[$i] VARCHAR(50) NOT NULL AFTER Id");
        }

        if(!in_array('color', $columns)) {
            $connect->query("ALTER TABLE colors.$nameTable ADD COLUMN color VARCHAR(50) NOT NULL");
        }
    }

    public function insertValuesIntoTable(string $nameTable, array $values, string $nameColumns, $color = '#FFFFFF')
    {
        $connect = ConnectDB::connectDB();
        $results = implode("', '", $values) ;

        echo 'addRow';

        if ($values[count($values) - 1][0] === '#') {
            $connect->query("INSERT INTO colors.$nameTable ($nameColumns, color) VALUES ('$results')");

        } else {
            $connect->query("INSERT INTO colors.$nameTable ($nameColumns) VALUES ('$results')");
        }
        Router::redirectPage('showDatabase');
    }
}