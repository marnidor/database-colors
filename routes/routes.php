<?php

use App\services\Router;

Router::openPage('/', 'home');
Router::openPage('/showDatabase', 'showDatabase');
Router::openPage('/updateDataInTable', 'updateDataInTable');
Router::openPage('/updateInsert', 'updateInsert');
Router::openPage('/addInsert', 'addInsert');
Router::openPage('/addNewRow', 'addNewRow');


$idUpdate = strripos($_SERVER['REQUEST_URI'], '/updateDataInTable?id=');

$idDelete = strripos($_SERVER['REQUEST_URI'], '/deleteRow?id=');


if (is_int($idUpdate)) {
    Router::openPage($_SERVER['REQUEST_URI'], 'updateDataInTable');
    Router::openPage('/updateInsert', 'updateInsert');
}

if (is_int($idDelete)) {
    \App\database\DataFromDB::deleteRowInTable($_GET['id'], $_SESSION['tableName']);
    Router::openPage($_SERVER['REQUEST_URI'], 'showDatabase');

}
//Router::post($_SERVER['REQUEST_URI'], \App\database\DataFromDB::class, 'updateDataInTable', true);
Router::openPage('/addNewPerson', 'addNewPerson');
//Router::post();
Router::compareUri();

